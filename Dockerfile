FROM ubuntu:22.04

ENV BUNDLE_PATH=/projects/gembox

WORKDIR /projects
RUN apt update && \
    apt install -y \
            build-essential \
            gosu \
            ruby-full \
            webp \
            zlib1g-dev && \
    useradd --create-home \
            -d /home/ruby-user \
            --uid 1000 \
            --shell /bin/bash \
            ruby-user && \
    chown -R ruby-user:1000 /home/ruby-user && \
    chmod -R 755 /home/ruby-user

RUN gem install bundler \
                jekyll \
                jekyll-admin \
                jekyll-feed \
                just-the-docs \
                minima \
                webrick

COPY ./entrypoint.sh /entrypoint
COPY ./convert-to-webp.sh /usr/local/bin/convert-to-webp


EXPOSE 4000

ENTRYPOINT ["/entrypoint"] # use CMD to override exec command
                
# override ENTRYPOINT to avoid gem management

# docker-jekyll

**Note:** jekyll is installed as root inside the container and is only
intended to be run locally for building and testing.


## Quick Start

If you want to get started playing/working right away, you can make use
of the `default` site packaged with this repository. It includes the
basic configurations necessary to utilize `jekyll-admin` as a frontend.

Build the `docker-jekyll` container:

```
docker build -t docker-jekyll .
```

Run Jekyll:

```
docker run --rm \
           -it \
           -v $(pwd)/projects:/projects \
           --workdir=/projects/default \
           -p '4000:4000' \
           docker-jekyll
```

You should now be able to reach http://localhost:4000/admin in your
browser.
![jekyll-admin screenshot](jekyll-admin_screenshot.png)


Your project files will live in `projects/default`. You can
edit and add to these files as needed to build out your custom site.




Alternatively, it's never a bad idea to start fresh! Below are
instructions on setting up a new site from scratch.


# Getting Setup

Clone this repository and then build the image with this command:

```
docker build -t docker-jekyll .
```

## docker-compose.yml

After you've built the docker image, copy the code below into a file named `docker-compose.yml` in the root of your project.
This configuration puts the root of your Jekyll project in `./site`.
This is where your `Gemfile`, `_config.yml`, and markdown will all live for your site.


```
version: "3"

services:
 docker-jekyll:
    image: docker-jekyll
    volumes:
      - ./:/projects
    working_dir: /projects/site
    ports:
      - '4000:4000'
```



## Create New Site

```
docker run --rm \
           -it \
           -v $(pwd):/projects \
           docker-jekyll \
           jekyll new site
```
This will create a new jekyll site in a folder named `site`.
This can be changed to something else, but the folder name is little significance.
Just make sure that if you use a different name you reference that same name in places where `site` is specified (i.e. docker-compose.yml).


Once the site is created, you may want to edit the `_config.yml` file
next to add your site title and any gems you may need for the site. If
the gems are not yet added to the docker image, this could be done
first. Gems need to be added to your site's `Gemfile` before they can be
served up by jekyll. After adding gems to your `Gemfile`, run:

```
docker run --rm \
           -it \
           -v $(pwd):/projects \
           --workdir=/projects/site \
           docker-jekyll \
           bundle install
```

## Starting up Jekyll

When running `docker-compose` commands, be certain you are in the directory where the docker-compose.yml are located.
This will keep your terminal session attached to the container so you can watch the log output as needed.
If you would like to detach (return to a prompt), just append `-d` on the end of the following command.

```
docker-compose up
```




## Webp

This Dockerfile contains command named `convert-to-webp` which can be
used to batch convert your .jpg, .jpeg, and .png files to .webp. This
script will remove all original files which have been converted to webp.
Back up your images before running this if you might want to undo!

**Example:**

```
convert-to-webp assets/images/ 60
```




**References:**

- https://jekyllrb.com/docs/
- https://github.com/just-the-docs/just-the-docs
- https://github.com/planetjekyll/awesome-jekyll-editors
- https://github.com/jekyll/jekyll-admin
- https://jekyll.github.io/jekyll-admin/self-hosting
- https://github.com/jekyll/jekyll-admin/issues/620
- https://github.com/decaporg/decap-cms
- https://decapcms.org/docs/intro/
- https://github.com/lexoyo/Jekyll-Silex
- https://stackoverflow.com/questions/65989040/bundle-exec-jekyll-serve-cannot-load-such-file

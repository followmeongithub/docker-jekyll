#!/bin/bash
set -e

# Params
if [ $# -lt 2 ]
  then
    echo "Usage:"
    echo -e "    convert-to-webp [IMAGE_DIRECTORY] [QUALITY]\n"
    echo "Example:"
    echo -e "    convert-to-webp assets/images/ 80\n"
    exit
fi
image_dir=$1
quality=$2



echo -e "\n\nCAUTION!!!! This will premanently delete your old images files after converting."
echo "Pausing here for 15 seconds in case you change your mind..."
echo -e "\n\nPress CTRL+C to quit!\n\n"
sleep 15


echo "moving into the ${image_dir} folder..."
cd $image_dir
total_starting_bytes=$(du -s |awk '{print $1}')

for image_file in $(ls |grep 'jpeg\|jpg\|png'); do
    echo "Converting $image_file to ${image_file%.*}.webp..."
    cwebp -q $quality ${image_file} -o ${image_file%.*}.webp
    echo "Removing ${image_file}..."
    rm $image_file
done


total_ending_bytes=$(du -s |awk '{print $1}')
total_bytes_saved=$(expr $total_starting_bytes - $total_ending_bytes)

echo "All .jpg, .jpeg, and .png files in $image_dir should now be converted!"
echo "Total bytes saved: $total_bytes_saved"

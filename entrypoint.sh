#!/bin/bash
#
# Entrypoint script checks/enforces permissions on projects folder
# and installs gems in the gembox (BUNDLE_PATH) when starting

SITE_PATH=${SITE_PATH:-/projects/site}


# Check /projects volume permissions
if ! [[ "$(ls -lan /projects |awk '{print $3 $4}')" == "10001000" ]];then
  echo "Fixing ownership of projects folder..."
  chown -R 1000:1000 /projects
fi

# Check if gem folder exists
#if ! [[ -d $BUNDLE_PATH ]];then
#  echo "Running `bundle install` and creating ${BUNDLE_PATH}..."
#  cd $SITE_PATH
#  gosu ruby-user bundle install
#fi


# run custom command or default to running jekyll
if [[ -z "$@" ]];then
  exec gosu ruby-user bundle exec jekyll serve --host=0.0.0.0
else
  exec gosu ruby-user $@
fi
